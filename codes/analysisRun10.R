# analysisRun10.R
# Paul Aiyetan
# April 26, 2016



# from a list object of the expression matrices (including respective phenotype data.frame)
#    this analysisRun generates expressionSet objects for respective dataset...

rm(list=ls())
load(file="./objs/datasetsExpMatAndPhenoDataList.rda")
require(parallel)
require(Biobase)

cl <- makeCluster(getOption("cl.cores",7))
t1 <- proc.time()
datasetsExpressionSets <-
    parLapply(cl,datasetsExpMatAndPhenoDataList,function(x){
        library(Biobase)
        eset <- ExpressionSet(assayData=as.matrix(x$expMat),
                              phenoData=new("AnnotatedDataFrame",
                                            data=x$pData))
        return(eset)
      })
t2 <- proc.time()
names(datasetsExpressionSets) <- names(datasetsExpMatAndPhenoDataList)

# save
save(datasetsExpressionSets,file="./objs/datasetsExpressionSets.rda")
load(file="./objs/datasetsExpressionSets.rda")




# t2-t1
#   user  system elapsed 
#  1.260   0.432   4.304 
