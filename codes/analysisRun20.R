# analysisRun20.R
# Paul Aiyetan
# May 18, 2016



# Employing the empirical baye's approach, this run of analysis merges the all of our study's dataset
#     while correcting inherent batches in our datasets.....
# Essentially, this run repeats the steps in "analysisRun14.R",
#      however in this case all datasets are used....

rm(list=ls())
library(Biobase)
library(inSilicoMerging)
library(sva)
load(file="./objs/allDatasetsExpressionSets.rda",verbose=TRUE)


# merge and EBayes-correct for batch effects
#  allMergedExpressionSetEBayesAdjusted.insilico <- merge(allDatasetsExpressionSets,method="COMBAT")
#  INSILICOMERGING: Run COMBAT...
#  INSILICOMERGING:   => Found 25 batches
#  INSILICOMERGING:   => Found 0 covariate(s)
#      Error in while (change > conv) { : missing value where TRUE/FALSE needed
# save(allMergedExpressionSetEBayesAdjusted,file="./objs/allMergedExpressionSetEBayesAdjusted.rda")


allMergedExpressionSetNoBatchAdjustment <- merge(allDatasetsExpressionSets,method="NONE")
save(allMergedExpressionSetNoBatchAdjustment,file="./objs/allMergedExpressionSetNoBatchAdjustment.rda")# save
load(file="./objs/allMergedExpressionSetNoBatchAdjustment.rda")

##############################################################
# attempt to use sva package defined ComBat
# since an error is being returned with ComBat in
#    inSilicoMerging...
source("./codes/combineExpressionMatrices.R",verbose=TRUE)
source("./codes/combinePhenoData.R",verbose=TRUE)
source("./codes/combineExpressionSets.R",verbose=TRUE)
combinedExpressionSet <- combineExpressionSets(allDatasetsExpressionSets) 
batch=pData(combinedExpressionSet)$dataset.id
#cExpMat=exprs(combinedExpressionSet)

allMergedEBayesAdjustedExpMat <-
    ComBat(dat=exprs(combinedExpressionSet),
           batch=pData(combinedExpressionSet)$dataset.id,
           par.prior=TRUE,prior.plot=FALSE)
# Found 25 batches
# Adjusting for 0 covariate(s) or covariate level(s)
# Found 12739 Missing Data Values
# Standardizing Data across genes
# Fitting L/S model and finding priors
# Finding parametric adjustments
# Adjusting the Data

# plot estimate, parameter distribution....
tiff("./figs/allMergedEmpiricalBatchEffectDensityPlot.tiff",height=2500,width=2500,res=350)
allMergedEBayesAdjustedExpMat2 <-
    ComBat(dat=exprs(combinedExpressionSet),
           batch=pData(combinedExpressionSet)$dataset.id,
           par.prior=TRUE,prior.plot=TRUE)#### visua
dev.off()

allMergedExpressionSetEBayesAdjusted <-
    ExpressionSet(assayData=allMergedEBayesAdjustedExpMat,
                  phenoData=new("AnnotatedDataFrame",
                                data=pData(combinedExpressionSet)))

save(allMergedExpressionSetEBayesAdjusted,file="./objs/allMergedExpressionSetEBayesAdjusted.rda")
load(file="./objs/allMergedExpressionSetEBayesAdjusted.rda")
