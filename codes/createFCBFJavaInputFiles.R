# createFCBFJavaInputFiles.R
# Paul Aiyetan
# July 11, 2016



# Similar to the createWekaARFFInputFile... this function creates an input file object for the
#    original java implementation of the FCBF algorithm by Ravi Bhimavarapu and Huan Liu (April, 2004).
#    reference: Lei Yu and Huan Liu (2004). Efficient Feature Selection via Analysis of Relevance and
#    Redundancy. Journal Machine Learning Research 5 (2004) 1205-1224


# NOTE: input to the FCBF algorithm is expected to be discretized data...Therefore, ensure that the input data
# table being supplied to the function is that which contatins data that have been discretized 
#   - any method of discretization is acceptable in this implementation. Discretization has been 
#     empirically found to produce better predictive results than undiscrestized data......

createFCBFJavaInputFiles <-
    function(objWithClassInfo, outputFilePath){ # outputFilePath=='stem' of the two output files...
        # the input to Ravi's FCBF java implementation is specified in two files. One containing the data
        #   value (with .data extension), while the other contains the metadata information to the data
        #   (with the .rc extension)

        # the .data file...
        #   contains each instance on a row and the attributes are comma seperated. The class label is given
        #   is given at the end of each instance
        pb <- txtProgressBar(min=0, max=100,initial=0,style=3)
        write(paste(objWithClassInfo[1,],collapse=",",sep=""),
              file=paste(outputFilePath,".data",collapse="", sep=""))
        setTxtProgressBar(pb,1)
        
        if(nrow(objWithClassInfo)>1){
            for(i in 2:nrow(objWithClassInfo)){
                write(paste(objWithClassInfo[i,],collapse=",",sep=""),
                      file=paste(outputFilePath,".data",collapse="", sep=""),
                      append=TRUE)
                setTxtProgressBar(pb, 2 + ((i/nrow(objWithClassInfo)) * 48)) 
            }
        }
        setTxtProgressBar(pb,51)
        
        # the .rc file...
        # write 'no of classes', class labels.
        write(paste(paste(length(unique(objWithClassInfo[,ncol(objWithClassInfo)])),
                          paste(unique(objWithClassInfo[,ncol(objWithClassInfo)]),sep="",collapse=","),
                          collapse=",",sep=","),
                    ".",sep="",collapse=""),
              file=paste(outputFilePath,".rc",collapse="", sep=""))
        setTxtProgressBar(pb,52)
        
        # write no of attributes
        write((ncol(objWithClassInfo)-1),
              file=paste(outputFilePath,".rc",collapse="",sep=""),append=TRUE)
        setTxtProgressBar(pb,53)
        
        # write attribute, attribute-values.
        for(i in 1:(ncol(objWithClassInfo)-1)){
            write(paste(colnames(objWithClassInfo)[i],
                        " - ",
                        paste(unique(objWithClassInfo[,i]),collapse=",",sep=""),
                        ".", collapse="",sep=""),
                  file=paste(outputFilePath,".rc",collapse="",sep=""),append=TRUE)
            setTxtProgressBar(pb, 53 + ((i/nrow(objWithClassInfo)) * 47))
        }

        return(TRUE)
    }
