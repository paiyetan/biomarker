# analysisRun03.R
# Paul Aiyetan
# April 05, 2016


load(file="./objs/testDataset.rda",verbose=TRUE)
# other potential [generic] class dicovery approaches +/- evaluation of performance of each
# 1.) Principal Component Analysis
library(ggbiplot)
features <- unique(unlist(testDataset$groupsSelectedFeatureRows))
testDatasetPCA <- prcomp(testDataset$dataMatrix)
# plot principal component
g <- ggbiplot(testDatasetPCA)
g <- g + scale_color_discrete(name="")
g <- g + theme(legend.direction="horizontal",legend.position="top")
png(filename="./figs/testDatasetPCA.FeatureSelected.png",height=2500,width=2500,res=350)
print(g)
dev.off()

